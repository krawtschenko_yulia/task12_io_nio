package me.krawtschenko.yulia.model;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UnreadInputStream extends FilterInputStream {
    private int bufferSize;
    private byte[] buffer;
    private int bufferPosition;

    public UnreadInputStream(InputStream input, int bufferSize) {
        super(input);

        this.bufferSize = bufferSize;
        buffer = new byte[bufferSize];
        bufferPosition = 0;
    }

    @Override
    public int read() throws IOException {
        if (bufferPosition > 0) {
            bufferPosition--;
            int read = buffer[bufferPosition];

            return read;
        } else {
            return super.read();
        }
    }

    public void unread(int read) throws IOException {
        if (bufferPosition >= bufferSize) {
            throw new IOException("Buffer full");
        } else {
            buffer[bufferPosition] = (byte) read;
            bufferPosition++;
        }
    }
}
