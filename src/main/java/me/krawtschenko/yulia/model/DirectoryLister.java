package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.stream.Collectors;

public class DirectoryLister {
    public static List<Path> list(Path currentDirectory) throws IOException {
        return Files.list(currentDirectory).collect(Collectors.toList());
    }

    public static List<BasicFileAttributes> getAttributes(List<Path> paths)
            throws IOException {
        return paths.stream()
                .map(path ->
                {
                    try {
                        return Files.readAttributes(path, BasicFileAttributes.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList()) ;
    }
}

