package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
    private InetSocketAddress socketAddress;
    SocketChannel socketChannel;

    public Client() throws IOException {
        socketAddress = new InetSocketAddress("localhost", 8080);
        socketChannel = SocketChannel.open();
    }

    public void send(String message) throws IOException, InterruptedException {
        byte[] messageBytes = message.getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(messageBytes);

        socketChannel.write(buffer);

        buffer.clear();
        Thread.sleep(2000);
    }

    public void close() throws IOException {
        socketChannel.close();
    }
}
