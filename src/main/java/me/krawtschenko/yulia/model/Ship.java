package me.krawtschenko.yulia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Ship<T extends Serializable> implements Serializable {
    private double mass;
    private int capacity;
    private int reactorDrain;
    private transient double[] currentPosition;
    private static int shipCount;
    private ArrayList<T> cargo;

    public Ship(double mass, int capacity, int reactorDrain,
                ArrayList<T> cargo) {
        this.mass = mass;
        this.capacity = capacity;
        this.reactorDrain = reactorDrain;
        this.cargo = cargo;
        shipCount++;
    }

    public Ship(double mass, int capacity, int reactorDrain, T cargo) {
        this.mass = mass;
        this.capacity = capacity;
        this.reactorDrain = reactorDrain;
        this.cargo = new ArrayList<T>();
        this.cargo.add(cargo);
        shipCount++;
    }

    public ArrayList<T> getCargo() {
        return cargo;
    }

    @Override
    public String toString() {
        String lineSeparator = System.getProperty("line.separator");

        StringBuilder cargo = new StringBuilder();
        this.cargo.forEach(item -> cargo.append(item.toString()));

        return "Ship{" +
                "mass=" + mass +
                ", capacity=" + capacity +
                ", reactorDrain=" + reactorDrain +
                ", currentPosition=" + Arrays.toString(currentPosition) +

                lineSeparator +
                "cargo=" + lineSeparator + cargo.toString() +

                "shipCount=" + shipCount +
                '}';
    }
}
