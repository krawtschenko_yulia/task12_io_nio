package me.krawtschenko.yulia.model;

import java.io.Serializable;
import java.util.Arrays;

public class Droid implements Serializable {
    private int level;
    private String[] armor;
    private int hitpoints;
    private static int droidCount;

    public Droid (int level, String[] armor, int hitpoints) {
        this.level = level;
        this.armor = armor;
        this.hitpoints = hitpoints;
        droidCount++;
    }

    public Droid (int level, String armor, int hitpoints) {
        this.level = level;
        this.armor = new String[] {armor};
        this.hitpoints = hitpoints;
        droidCount++;
    }

    @Override
    public String toString() {
        String lineSeparator = System.getProperty("line.separator");

        return "Droid" + lineSeparator
                + "\tLevel: " + level + lineSeparator
                + "\tArmor: " + Arrays.toString(armor) + lineSeparator
                + "\tHitpoints: " + hitpoints + lineSeparator
                + "\tDroid count: " + droidCount + lineSeparator;
    }
}
