package me.krawtschenko.yulia.model;

import java.io.*;

public class ApplicationModel {
    public void copy(File from, File to) throws IOException {
        try (
            BufferedReader reader =
                    new BufferedReader(new FileReader(from));
            BufferedWriter writer = new BufferedWriter(new FileWriter(to))
        ) {
            String line;
            while((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
        }
    }

    public void copy(File from, File to, int bufferSize) throws IOException {
        try (
            BufferedReader reader = new BufferedReader(new FileReader(from),
                    bufferSize);
            BufferedWriter writer = new BufferedWriter(new FileWriter(to))
        ) {
            String line;
            while((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
        }
    }

    public long measurePerformance(MeasurablePerformance method)
            throws IOException {
        long start = System.nanoTime();
        method.execute();
        long finish = System.nanoTime();

        return finish - start;
    }
}
