package me.krawtschenko.yulia.model;

import java.io.IOException;

public interface MeasurablePerformance {
    void execute() throws IOException;
}
