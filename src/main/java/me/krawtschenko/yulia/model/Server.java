package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;

public class Server {
    private Selector selector;
    private InetSocketAddress socketAddress;
    private ServerSocketChannel serverChannel;

    public Server() throws IOException {
        selector = Selector.open();
        socketAddress = new InetSocketAddress("localhost", 8080);
        serverChannel = ServerSocketChannel.open();
        serverChannel.bind(socketAddress);
        serverChannel.configureBlocking(false);
    }

    private void startListening() throws IOException {
        int ops = serverChannel.validOps();
        serverChannel.register(selector, ops);

        while (true) {
            if (selector.select() == 0) {
                continue;
            }

            for (SelectionKey key : selector.selectedKeys()) {
                if (!key.isValid()) {
                    continue;
                }

                if (key.isAcceptable()) {
                    accept();
                } else if (key.isReadable()) {
                    read(key);
                } else if (key.isWritable()) {

                }
            }
        }
    }

    private void accept() throws IOException {
        SocketChannel socketChannel = serverChannel.accept();
        socketChannel.configureBlocking(false);

        socketChannel.register(selector, SelectionKey.OP_READ);
    }

    private String read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(512);

        socketChannel.read(buffer);

        return new String(buffer.array()).trim();
    }
}
