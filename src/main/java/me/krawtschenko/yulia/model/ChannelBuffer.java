package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;

public class ChannelBuffer {
    private ReadableByteChannel input;
    private WritableByteChannel output;
    private ByteBuffer buffer;
    private Charset charset = Charset.defaultCharset();

    public ChannelBuffer(ReadableByteChannel input, WritableByteChannel output,
                         int bufferSize) {
        this.input = input;
        this.output = output;
        buffer = ByteBuffer.allocateDirect(bufferSize);
    }

    public void copy() throws IOException {
        while (input.read(buffer) != -1) {
            buffer.flip();
            while (buffer.hasRemaining()) {
                output.write(buffer);
            }
            buffer.clear();
        }
    }
}
