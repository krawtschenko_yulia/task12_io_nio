package me.krawtschenko.yulia.model;

import java.util.Scanner;

public class CommentExtractor {
    private Scanner reader;

    private int numberDoubleQuotes = 0;

    private boolean insideBlockComment = false;
    private boolean insideInlineComment = false;

    public CommentExtractor(Scanner reader) {
        this.reader = reader;
    }

    public String read() {
        StringBuilder stringToBe = new StringBuilder();
        String line;
        String lineSeparator = System.getProperty("line.separator");

        while (reader.hasNextLine()) {
            line = reader.nextLine();
            System.out.println(line);
            if (line != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == '"') {
                        numberDoubleQuotes++;
                    }

                    // not inside a string
                    if (isEven(numberDoubleQuotes)) {
                        if (i + 1 < line.length()
                                && line.charAt(i) == '/'
                                && line.charAt(i + 1) == '*') {
                            insideBlockComment = true;
                            i++;
                        }
                        if (i + 1 < line.length()
                                && line.charAt(i) == '*'
                                && line.charAt(i + 1) == '/') {
                            insideBlockComment = false;
                            stringToBe.append(lineSeparator);
                            i++;
                        }

                        if (!insideBlockComment) {
                            if (i + 1 < line.length()
                                    && line.charAt(i) == '/'
                                    && line.charAt(i + 1) == '/') {
                                insideInlineComment = true;
                                i++;
                            }
                        }
                    }

                    if (insideBlockComment || insideInlineComment) {
                        stringToBe.append(line.charAt(i));
                    }
                }

                // new line ends inline comment
                if (insideInlineComment) {
                    stringToBe.append(lineSeparator);
                }
                insideInlineComment = false;
            }
        }
        return stringToBe.toString();
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }
}
