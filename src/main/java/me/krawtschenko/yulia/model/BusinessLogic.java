package me.krawtschenko.yulia.model;

import java.io.*;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class BusinessLogic {
    private ApplicationModel model;

    public BusinessLogic() {
        model = new ApplicationModel();
    }

    public Ship<Droid> createShipWithDroids() {
        Droid armekMilitaryGradeDroid = new Droid(
                3,
                new String[]{"Metal", "Mineral"},
                60
        );
        Droid allofreeDroid = new Droid(
                1,
                "Metal",
                20
        );
        Droid qualdexEnhancedDroid = new Droid(
                9,
                new String[]{"Metal", "Mineral", "Chemical"},
                240
        );

        ArrayList<Droid> droidCargo = new ArrayList<Droid>();
        droidCargo.add(armekMilitaryGradeDroid);
        droidCargo.add(allofreeDroid);
        droidCargo.add(qualdexEnhancedDroid);

        Ship<Droid> havocStarfighter = new Ship<Droid>(
                2346.6,
                250,
                180,
                droidCargo
        );

        return havocStarfighter;
    }

    public Ship<Droid> shipDroidsAround(Ship<Droid> ship) throws IOException,
            ClassNotFoundException {
        FileOutputStream output = new FileOutputStream("droidship.ser");
        ObjectOutputStream objectOutput = new ObjectOutputStream(output);
        objectOutput.writeObject(ship);

        FileInputStream input = new FileInputStream("droidship.ser");
        ObjectInputStream objectInput = new ObjectInputStream(input);
        Object read = objectInput.readObject();
        Ship readShip;

        if (read instanceof Ship) {
            readShip = (Ship) read;
            Object cargoSample = readShip.getCargo().get(0);
            if (cargoSample instanceof Droid) {
                return (Ship<Droid>) readShip;
            }
        } else {
            throw new RuntimeException("Not a droid ship");
        }
        return null;
    }

    public HashMap<Integer, Long> comparePerformance(File source, File destination) throws IOException {
        MeasurablePerformance copyWithoutBuffer = () ->
                model.copy(destination, source);
        HashMap<Integer, MeasurablePerformance> copyWithBuffers
                = new HashMap<>();

        for (int powerOfTwo = 32; powerOfTwo < 2048; powerOfTwo *= 2) {
            int bufferSize = powerOfTwo;
            MeasurablePerformance copyWithBuffer = () ->
                    model.copy(destination, source, bufferSize);

            copyWithBuffers.put(bufferSize, copyWithBuffer);
        }

        HashMap<Integer, Long> executionTimes = new HashMap<>();
        executionTimes.put(0, model.measurePerformance(copyWithoutBuffer));

        for (Map.Entry<Integer, MeasurablePerformance> entry
                : copyWithBuffers.entrySet()) {
            Integer bufferSize = entry.getKey();
            MeasurablePerformance method = entry.getValue();
            executionTimes.put(bufferSize, model.measurePerformance(method));
        }
        return executionTimes;
    }

    public String demonstrateUnreadingInputStream(String filename)
            throws IOException {
        FileInputStream fileInput = new FileInputStream(filename);
        UnreadInputStream stream = new UnreadInputStream(fileInput, 8);

        StringBuilder stringToBe = new StringBuilder();
        int read;

        while ((read = stream.read()) != -1) {
            char ch = (char) read;
            if (ch == '*') {
                stream.unread('#');
            } else {
                stringToBe.append(ch);
            }
        }
        return stringToBe.toString();
    }

    public String readComments(Path file) throws IOException {
        Scanner reader = new Scanner(file);
        CommentExtractor commentReader = new CommentExtractor(reader);

        return commentReader.read();
    }

    public void copyViaChannelBuffer(Path inputFile, Path outputFile)
            throws IOException {
        SeekableByteChannel inputChannel = Files.newByteChannel(inputFile);

        Set options = new HashSet();
        options.add(StandardOpenOption.CREATE);
        options.add(StandardOpenOption.APPEND);
        SeekableByteChannel outputChannel
                = Files.newByteChannel(outputFile, options);

        ChannelBuffer buffer = new ChannelBuffer(inputChannel, outputChannel,
                512);

        buffer.copy();

        inputChannel.close();
        outputChannel.close();
    }

    public List<String> readDirectoryContents (String directoryPath)
            throws IOException {
        DirectoryViewer directoryViewer = new DirectoryViewer(directoryPath);

        return directoryViewer.getDirectoryData();
    }

}