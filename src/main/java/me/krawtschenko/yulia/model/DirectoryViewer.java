package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class DirectoryViewer {
    private Path directory;

    public DirectoryViewer(Path directory) {
        this.directory = directory;
    }

    public DirectoryViewer(String directoryName) {
        directory = Paths.get(directoryName);
    }

    public void setDirectory(Path directory) {
        this.directory = directory;
    }

    public void setDirectory(String directoryName) {
        directory = Paths.get(directoryName);
    }

    public List<String> getDirectoryData()
            throws IOException {
        List<String> directoryData = new ArrayList<>();

        List<Path> list = DirectoryLister.list(directory);
        List<BasicFileAttributes> data = DirectoryLister.getAttributes(list);

        for (int i = 0; i < list.size(); i++) {
            int last = list.get(i).getNameCount() - 1;
            directoryData.add("Name: " + list.get(i).getName(last).toString());
            directoryData.add("Is a directory: " + data.get(i).isDirectory());
            directoryData.add("Is a regular file: " +
                    data.get(i).isRegularFile());
            directoryData.add("Is a symbolic link: " +
                    data.get(i).isSymbolicLink());
            directoryData.add("Size (bytes): " + data.get(i).size());
            directoryData.add("Created: " + data.get(i).creationTime());
            directoryData.add("Last accessed: " + data.get(i).lastAccessTime());
            directoryData.add("Last modified: " +
                    data.get(i).lastModifiedTime());
        }

        return directoryData;
    }
}
