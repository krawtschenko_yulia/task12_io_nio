package me.krawtschenko.yulia.view;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public enum MenuOption {
    QUIT("Q"),
    SERIALIZE_DESERIALIZE_OBJECTS("1"),
    COMPARE_READING_WRITING_PERFORMANCE("2"),
    USE_UNREAD_INPUT_STREAM("3"),
    READ_COMMENTS_FROM_SOURCE_FILE("4"),
    USE_CHANNEL_BUFFER("5"),
    LIST_DIRECTORY_CONTENTS("6"),
    START_SERVER("7"),
    START_CLIENT("8");

    private final String key;

    MenuOption(String key) {
        this.key = key;
    }

    MenuOption() {
        key = "";
    }

    public String getKeyToPress() {
        return key;
    }

    private static final Map<String, MenuOption> table = new HashMap<>();

    static {
        for (MenuOption option : MenuOption.values()) {
            table.put(option.getKeyToPress(), option);
        }
    }

    public static MenuOption get(String key) {
        return table.get(key);
    }
}

class OptionKeyComparator implements Comparator<MenuOption> {
    @Override
    public int compare(MenuOption o1, MenuOption o2) {
        return o1.getKeyToPress().compareTo(o2.getKeyToPress());
    }
}