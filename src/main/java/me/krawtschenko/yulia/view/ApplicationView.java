package me.krawtschenko.yulia.view;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ApplicationView extends View{
    public ApplicationView() {
        super();

        MenuItem quit = new MenuItem(MenuOption.QUIT, "Quit");
        menu.put(MenuOption.QUIT, quit);

        MenuItem useSerialization
                = new MenuItem(MenuOption.SERIALIZE_DESERIALIZE_OBJECTS,
                "Serialize/deserialize objects");
        menu.put(MenuOption.SERIALIZE_DESERIALIZE_OBJECTS, useSerialization);

        MenuItem comparePerformance
                = new MenuItem(MenuOption.COMPARE_READING_WRITING_PERFORMANCE,
                "Compare performance with/without buffers");
        menu.put(MenuOption.COMPARE_READING_WRITING_PERFORMANCE, comparePerformance);

        MenuItem useUnreadInputStream
                = new MenuItem(MenuOption.USE_UNREAD_INPUT_STREAM,
                "Use UnreadInputStream class " +
                        "to replace '*' with '#'");
        menu.put(MenuOption.USE_UNREAD_INPUT_STREAM, useUnreadInputStream);

        MenuItem readComments
                = new MenuItem(MenuOption.READ_COMMENTS_FROM_SOURCE_FILE,
                "Read comments from a source file");
        menu.put(MenuOption.READ_COMMENTS_FROM_SOURCE_FILE, readComments);

        MenuItem useChannelBuffer
                = new MenuItem(MenuOption.USE_CHANNEL_BUFFER,
                "Use channel buffer");
        menu.put(MenuOption.USE_CHANNEL_BUFFER, useChannelBuffer);

        MenuItem listDirectoryContents =
                new MenuItem(MenuOption.LIST_DIRECTORY_CONTENTS,
                "List contents of a given directory");
        menu.put(MenuOption.LIST_DIRECTORY_CONTENTS, listDirectoryContents);

        MenuItem startServer
                = new MenuItem(MenuOption.START_SERVER,
                "Start server");
        menu.put(MenuOption.START_SERVER, startServer);

        MenuItem startClient
                = new MenuItem(MenuOption.START_CLIENT,
                "Start client");
        menu.put(MenuOption.START_CLIENT, startClient);
    }

    public int readInt() {
        return input.nextInt();
    }

    public double readDouble() {
        return input.nextDouble();
    }

    public <K, V> void printMap(Map<K, V> map) {
        map.forEach((key, value) -> {
            System.out.println(key + ": " + value);
        });
    }

    public <T> void printSet(Set<T> set) {
        set.forEach(System.out::println);
    }

    public <T> void printArray(T[] array) {
        for (T element : array) {
            System.out.println(element);
        }
    }

    public <T> void printList(List<T> list) {
        list.forEach(item -> {
            System.out.println(item);
        });
    }

    @Override
    public void displayMenu() {
        super.printMessage("MENU");
        super.displayMenu();
    }

    private boolean isValidLocale(String string) {
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale locale : locales) {
            if (string.equals(locale.toString())) {
                return true;
            }
        }
        return false;
    }
}