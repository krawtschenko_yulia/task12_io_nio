package me.krawtschenko.yulia.view;

@FunctionalInterface
public interface MenuAction {
    void execute();
}