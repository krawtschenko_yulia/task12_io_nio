package me.krawtschenko.yulia.view;

public class MenuItem {
    private MenuOption key;
    private String description;
    private MenuAction action;

    MenuItem(MenuOption key, String description) {
        this.key = key;
        this.description = description;
    }

    public void linkAction(MenuAction action) {
        this.action = action;
    }

    public MenuOption getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public MenuAction getAction() {
        return action;
    }
}