package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BusinessLogic;
import me.krawtschenko.yulia.model.Droid;
import me.krawtschenko.yulia.model.Ship;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.MenuAction;
import me.krawtschenko.yulia.view.MenuOption;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class ApplicationController {
    private BusinessLogic model;
    private ApplicationView view;

    private String filename = null;

    private MenuAction useSerialization;
    private MenuAction comparePerformance;
    private MenuAction useUnreadInputStream;
    private MenuAction readComments;
    private MenuAction useChannelBuffer;
    private MenuAction viewDirectory;
    private MenuAction startServer;
    private MenuAction startClient;

    private final Logger logger = LogManager
            .getLogger(ApplicationController.class);

    public ApplicationController() {
        try {
            model = new BusinessLogic();
            view = new ApplicationView();

            useSerialization = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        Ship<Droid> droidShip = model.createShipWithDroids();
                        view.printMessage("Before serialization:");
                        view.print(droidShip);

                        Ship<Droid> shipAfterSerialization
                                = model.shipDroidsAround(droidShip);
                        view.printMessage("After serialization " +
                                "(with casting):");
                        view.print(shipAfterSerialization);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.SERIALIZE_DESERIALIZE_OBJECTS,
                    useSerialization);

            comparePerformance = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        File source = new File("source.txt");
                        File destination = new File("destination.txt");

                        HashMap<Integer, Long> executionTimes
                                = model.comparePerformance(source, destination);
                        view.printMessage("Time taken to copy a file:");
                        view.print(executionTimes);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(
                    MenuOption.COMPARE_READING_WRITING_PERFORMANCE,
                    comparePerformance
            );

            useUnreadInputStream = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        String filename = "sample.txt";
                        view.printMessage("File contents read with " +
                                "UnreadInputStream:");
                        view.print(model.demonstrateUnreadingInputStream(filename));
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.USE_UNREAD_INPUT_STREAM,
                    useUnreadInputStream);

            readComments = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        while (filename == null || filename.length() == 0) {
                            view.printMessage("No input file provided.");
                            view.printMessage("Enter path to the input file:");
                            filename = view.readLine();
                        }
                        Path file = Paths.get(filename);
                        view.printMessage("Comments found in the input file:");
                        view.print(model.readComments(file));
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.READ_COMMENTS_FROM_SOURCE_FILE,
                    readComments);

            useChannelBuffer = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        Path source = Paths.get("source.txt");
                        Path destination = Paths.get("destination.txt");

                        model.copyViaChannelBuffer(source, destination);

                        view.printMessage("Copied from " + source + " to "
                                + destination);
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.USE_CHANNEL_BUFFER,
                    useChannelBuffer);

            viewDirectory = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        String currentDirectory = ".";
                        String command;

                        do {
                            view.printMessage("Enter command (cd/dir) or \"exit\" to quit:");
                            command = view.readLine();

                            if (command.strip().equals("dir")) {
                                view.printMessage("Directory " + currentDirectory);
                                view.printList(model
                                        .readDirectoryContents(currentDirectory));
                            } else if (command.strip().startsWith("cd")) {
                                currentDirectory = command
                                        .replace("cd", " ")
                                        .strip();
                                view.printMessage("New directory: " + currentDirectory);
                            } else if (command.strip().equals("exit")) {
                                break;
                            } else {
                                view.printMessage("Unknown command");
                            }
                        } while (!command.equals("exit"));
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.LIST_DIRECTORY_CONTENTS,
                    viewDirectory);

            startServer = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        view.printMessage("");
                        view.print("");
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.START_SERVER,
                    startServer);

            startClient = new MenuAction() {
                @Override
                public void execute() {
                    try {
                        view.printMessage("");
                        view.print("");
                    } catch (Exception ex) {
                        logger.error(ex);
                    }
                }
            };
            view.addMenuItemAction(MenuOption.START_CLIENT,
                    startClient);

            MenuAction quit = () -> System.exit(0);
            view.addMenuItemAction(MenuOption.QUIT, quit);
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    public void startMenu() {
        try {
            view.displayMenu();
        } catch (RuntimeException ex) {
            logger.error(ex.getStackTrace());
        }
    }

    public void setFileName(String filename) {
        this.filename = filename;
    }

    private String formatObjectAndTypeString(Object object) {
        return String.format("%s (%s)", object.toString(),
                object.getClass().getName());
    }
}