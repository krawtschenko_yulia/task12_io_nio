package me.krawtschenko.yulia;

import me.krawtschenko.yulia.controller.ApplicationController;

public class Application {
    public static void main(String[] args) {
        ApplicationController controller = new ApplicationController();
        if (args.length == 1) {
            controller.setFileName(args[0]);
        }
        controller.startMenu();
    }
}